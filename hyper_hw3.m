clear;clc;close all;

%% Crop a 256x256 part from the hyperspectral image that you have constructed.
x = hdfinfo('E:\homework\EO1H1740352005011110PI\EO1H1740352005011110PI.L1R');
sds_info = x.SDS
hyp_img = hdfread(sds_info);

%hyperspectral image
img = rand(3352,256,242);
img = int16(img);
for i=1:242
    pseudo_band = hyp_img(:,i,:);
    pseudo_band_squeeze = squeeze(pseudo_band);
    img(:,:,i)= pseudo_band_squeeze; 
end

% Crop a 256x256 part
img_256 = img(1850:2105,1:256,:);
% Draw the scatter plot of two consecutive pair of bands.
xBand = img_256(1,1:256,16);
yBand = img_256(1,1:256,17);
figure 
hold on
xlabel('Band 16')
ylabel('Band 17')
plot(xBand,yBand, '.')

%% Calculate a 2D histogram of the same two bands 
%(use whole image) using basic functions. 
band16 = img_256(1:256,1:256,16);
band17 = img_256(1:256,1:256,17);

normband16 = round(mat2gray(band16)*100);
minx = min(normband16);
normband16(normband16<1) = minx(256);
normband16(normband16>100) = 100;

normband17 = round(mat2gray(band17)*100);
minx = min(normband17);
normband17(normband17<1) = minx(256);
normband17(normband16>100) = 100;

bnd16 = reshape(normband16,[65536,1]);
bnd17 = reshape(normband17,[65536,1]);

Histogram2D = accumarray([bnd16 bnd17], 1, [100 100]);
figure 
hold on
subplot(1,2,1)
mesh(Histogram2D)
subplot(1,2,2)
contour(Histogram2D)

%% Calculate the 242x242 covariance matrix of the image using matrix operations
%covariance matrix
q = ones(65536,242);
for i = 1:242
    q(:,i) = mat2gray(reshape(img_256(:,:,i),[65536,1]));
end
q_mean = sum(q) / size(q,1);
q_mean_subtract = q - q_mean(ones(256,256), :);
cov_matrix = (q_mean_subtract.' * q_mean_subtract) / (size(q,1) - 1);

%correlation matrix
for i = 1:242
    for j = 1:242
    R(i,j) = cov_matrix(i,j) / sqrt(cov_matrix(i,i)*cov_matrix(j,j));
    end
end

figure 
hold on 
subplot(1,2,1)
imagesc(cov_matrix)
title('Covariance Matrix')
subplot(1,2,2)
imagesc(R)
title('Corelation Matrix')

%% Calculate eigenvalues using the function eig
[eigenVec,eigenVal] = eigs(cov_matrix,242);
PC1 = q*eigenVec(:,1); 
PC1 = reshape(PC1(:,:),[256,256,1]);
PC2 = q*eigenVec(:,2); 
PC2 = reshape(PC2(:,:),[256,256,1]);

% optional
% PC3 = q*eigenVec(:,3); 
% PC3 = reshape(PC3(:,:),[256,256,1]);

subplot(1,2,1)
imagesc(PC1)
title('PC1')
subplot(1,2,2)
imagesc(PC2);
title('PC2')
legend('0','5','10')

figure
subplot(1,2,1)
plot(PC1(1,1:256),PC2(1,1:256),'.r')
title('PC Histogram')
xlabel('PC 1')
ylabel('PC 2')

subplot(1,2,2)
plot(eigenVal(1:20),'-o')
title('Scree Plot - How Variance changes')
xlabel('PC number')
ylabel('Eigenvalue')

